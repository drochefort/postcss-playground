# PostCSS Playground

Playing around with PostCSS.
Trying to replace Sass pre-processing with PostCSS

Here are the plugins we use:

- css linter, that is able to detect syntax errors
- precss : to compile Sass syntax to CSS
- sorting: to sort the css rules
- autoprefixer

Much more plugins are available.

- it's bleeding fast compared to sass
- it's pure javascript, doesn't require Ruby to be installed.