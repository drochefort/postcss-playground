module.exports = function(grunt) {
    require('load-grunt-tasks')(grunt);

    grunt.initConfig({
        postcss: {
            options: {
                map: true, // inline sourcemaps

                // or
                map: {
                    inline: false, // save all sourcemaps as separate files...
                    annotation: 'playground/css/maps/' // ...to the specified directory
                },

                syntax: require('postcss-scss'),

                processors: [
                    require('stylelint')({ configFile: './.stylelintrc' }),
                    require('precss'),
                    require('postcss-sorting')(), // add fallbacks for rem units
                    require('autoprefixer')({ browsers: 'last 2 versions' }), // add vendor prefixes
                ]
            },
            dist: {
                src: 'playground/sass/*.css',
                dest: 'playground/css/index.css'
            }
        }
    });


    grunt.registerTask('default', ['postcss']);
}
